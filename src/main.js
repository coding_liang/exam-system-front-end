import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/element.js'
import './assets/css/global.css'
// 导入iconfront
import './assets/font/iconfont.css'
// 导入axios
import axios from 'axios'
// 挂载axios 到Vue的原型prototype的$http
Vue.prototype.$http=axios
//axios.defaults.baseURL = 'api'
//axios.defaults.baseURL="http://localhost:8090/"

axios.defaults.baseURL="http://localhost:8090/"  //设置全局url
axios.interceptors.request.use(config => {
  console.log(config);
  // 请求头挂载信息
  config.headers.Authorization = window.sessionStorage.getItem("flag");
  // 在最后必须return config
  return config;
})



Vue.config.productionTip = false

new Vue({
  router,
  store,  // 初始化的时候就会引用
  render: h => h(App)
}).$mount('#app')
