import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from "../components/Login.vue"
import Home from "../components/Home.vue"
//import LoginTest from "../components/LoginTest.vue"
import Welcome from '../components/Welcome.vue'
import User from '../components/admin/User.vue'
 import examList from '../components/exam/examList.vue'
 import editExam from '../components/exam/editExam'
import publishExam from '../components/exam/publishExam'


import success from '../components/Success.vue'
import correctList from '../components/solution/correctList.vue'
import readed from'../components/solution/readed.vue'
import teacher from '../components/admin/teacher.vue'
import problemmList from'../components/question/questionList.vue'
import singleProblem from'../components/question/edit/single.vue'
import multipleProblem from '../components/question/edit/mmultiple.vue'
import file from'../components/File.vue'
import essayProblem from'../components/question/edit/essay.vue'
import judgmentProblem from'../components/question/edit/judgment.vue'
import updateExam from'../components/exam/updateExam.vue'
//后期还需要配置一些页面  404 之类的  还有不同权限的分配等等
Vue.use(VueRouter)

const routes = [
  {
    path: '/success',
    component: success
  },
  {
    path: '/login',
    component: Login
  },
 
  {
    path: '/home',
    component: Home,
    redirect: '/welcome',
    children: [
      { path:'/welcome', component: Welcome},
      { path:'/user', component: User},
      { path:'/teacher', component: teacher},
      { path:'/examList', component: examList},
      { path:'/createExam', component: editExam},
      { path:'/problemmList', component: problemmList},
      { path:'/singleProblem', component: singleProblem},
      { path:'/multipleProblem', component: multipleProblem},
      { path:'/correctList', component: correctList},
      { path:'/readed', component: readed},
      { path:'/file', component: file},
      { path:'/essayProblem', component: essayProblem},
      { path:'/judgmentProblem', component: judgmentProblem},
      { path:'/updateExam', component: updateExam},
      { path:'/publishExam', component: publishExam}

    ],
  }

]
/**
 * 写一个动态加载的路由 根据权限来控制路由的跳转
 * 考生的页面
 * 管理者的页面
 */
// export const DynamicRoutes=[

//   {
//     path:"",
//     Comment: Welcome,  //一些组件
//     name: "container",
//     redirect:"home",
//     meta:{
//       requiresAuth:true , //作用是验证是否需要登录才能打开
//       name:"首页",
//     },
//     children:[
//           {
//             path:"home",
//             Comment: Home,  //一些组件
//             name: "home",         
//                meta:{
//                  //需要增加一些匹配的规则
//               name:"首页",
//               icon:"icon-name" //要显示的图标
//             },
//           }
//     ]
    
//   },
//   {
//     path:"/403",
//     Component: "",//待添加
//   },
//   {
//     path:"*",  //所有没有匹配到的页面都是404
//     Component: "",//待添加
//   }
// ]
//这是固定的写法不用去管他
const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
// 挂载路由导航守卫
router.beforeEach((to, from, next) => {
  // to:将要访问的路径
  // from:从哪里访问的路径
  // next:之后要做的任务，是一个函数
  //    next（）放行， next（'/URL'）强制跳转的路径。
  if (to.path == '/login') return next();// 访问路径为登录
  // 获取flag
  const flagStr = window.sessionStorage.getItem("flag");// session取值
  if (!flagStr) return next('/login');// 没登录去登录 返回登录页面
  next(); //复合要求，则放行
})

export default router
