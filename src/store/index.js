import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
// import './plugins/element.js'
// import './assets/css/global.css'
import state from'./defaultState'
// 导入iconfront
// import './assets/font/iconfont.css'
import mutations from'./mutations'
Vue.use(Vuex)

// https://webpack.js.org/guides/dependency-management/#requirecontext
const modulesFiles = require.context('./modules', true, /\.js$/)

// you do not need `import app from './modules/app'`
// it will auto require all vuex module from modules file
const modules = modulesFiles.keys().reduce((modules, modulePath) => {
  // set './app.js' => 'app'
  const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
  const value = modulesFiles(modulePath)
  modules[moduleName] = value.default
  return modules
}, {})

export default new Vuex.Store({

  state :state,  //使用导入的state
  mutations,
  
  modules,
  getters

})
