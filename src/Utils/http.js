import axios from "axios";
import store from "@/store";
import { Message } from "element-ui";
import { reject } from "core-js/fn/promise";
const http={}

var instance=axios.create({
    timeout:500

})

instance.interceptors.request.use(


    function(config){
        //请求头添加token  有关于token的存储
        if(store.state.UserToken){
            config.headers.Authorization=store.state.UserToken
        }
        return config
    },
    function(error){
        return Promise.reject(error)
    }

)

//相应拦截器即异常处理
instance.interceptors.response.use(
    response=>{
        return response.data
    },
    //异常处理
    err=>{
        if(err&&err.response){
            switch(err.response.status){
                case 400:
                    err.Message='请求错误'
                    break
                case 401:
                    Message.warning({
                        message:'授权失败，请重新登录'
                    })
                    store.commit('LOGIN_OUT')
                    setTimeout(() => {
                        window.location.reload()
                    }, 1000)
                    return
                //还有很多的条件要去实现
            }
        }
            //放行？？
         return Promise.reject(response.message)


    }
)

http.get=function(url,options){

    //
}


export function get(url, params){
    return new Promise((resolve, reject) =>{
        axios.get(url, {
            params: params        
        }).then(res => {
            resolve(res.data);
        }).catch(err =>{
            reject(err.data)        
        })    
    })
}
/** 
 * post方法，对应post请求 
 * @param {String} url [请求的url地址] 
 * @param {Object} params [请求时携带的参数] 
 */
export function post(url, params) {
    return new Promise((resolve, reject) => {
         axios.post(url, QS.stringify(params))
        .then(res => {
            resolve(res.data);
        })
        .catch(err =>{
            reject(err.data)
        })
    });
}
